#!/bin/bash

# You're not supposed to be reading this
# But at the same time, you're also not supposed to run a random script
# you got off the Internet with admin permissions without checking it first
# So I guess props to you for figuring out how to get here and not running anything
# you don't know about. 
#
# Email me your favorite animal for some extra credit

function opening_prompt() {
    echo -ne "Connecting"
    sleep 1
    echo -ne "."
    sleep 1
    echo -ne "."
    sleep 1
    echo -ne ".\n"
    sleep 1
    echo -ne "Initializing"
    sleep 1
    echo -ne "."
    sleep 1
    echo -ne "."
    sleep 1
    echo -ne ".\n"
    sleep 2

    echo 'Welcome aboard, subject_name! Congratulations on your successful appointment to the position of Linux System Administrator!'
    sleep .1
    echo 'I think you’re going to love working for Logical Information Systems and Analytics Inc. (referred to as “LISA”)'
    sleep .1
    echo 'and are going to fit right into our team (“IT staff” consisting of the following roles: Linux System Administrator).'
    sleep .1
    read -p "[Press enter for more]"

    echo 'As LISA’s latest and greatest Linux System Administrator you will  be in charge of the entire company’s Linux computer'
    sleep .1
    echo 'systems. As part of this role, you are responsible for assisting any employee of LISA with any problem that falls within'
    sleep .1
    echo 'the scope of your position. '
    sleep .1
    read -p "[Press enter for more]"

    echo 'In order to more steamline the passage of knowledge from one team member to the next, we have implemented a policy of'
    sleep .1
    echo 'thorough documentation that we like to call “Documentation”. As part of this policy, every employee of LISA must have a'
    sleep .1
    echo 'complete understanding of everything that you do in your role.'
    sleep .1
    read -p "[Press enter for more]"

    echo 'Anyone from the janitor to your supervisor must be able to understand what you’re doing and why. By strictly conforming to'
    sleep .1
    echo 'this policy, in the event of your sudden disappearance, the company will be able to recreate anything you’ve done using'
    sleep .1
    echo 'your detailed instructions.'
    sleep .1
    read -p "[Press enter for more]"

    echo 'That’s all for now, new recruit! If you have any questions, don’t hesitate to reach out to your new '
    sleep .1
    echo 'family (“IT staff of LISA”). If you have any concerns, you can email HR at �e@ӯ��)�t)�� �N��w0ii'
    sleep .1
    read -p "[Press enter for more]"
    head /usr/bin/echo
    echo 'line 91234186: % : syntax error near unexpected token `]]: operand expected (error token is "$complaint_box")'
    sleep .1
    echo "$(tput bold; tput setaf 1) FATAL: UNREACHABLE => (\"Failed to connect to server: connected closed by administrator\", unreachable = TRUE)"
    sleep .1
    echo -ne 'No Connection'
    sleep 1
    echo -ne "."
    sleep 1
    echo -ne "."
    sleep 1
    echo -ne ".\n"
    echo "Disconecting"
    sleep 2
}

echo "Warning! This script may delete things in your home directory!"
echo "Are you sure you want to continue?"
#read -p "Type \"Yes, I am\" to continue setup: " user_input

#if [[ $user_input == "Yes, I am" ]]
#then
    USER=$(who | head -n1 | cut -d" " -f1)

    echo ""

    hostnamectl set-hostname LISA_46295
    eval rm -rf ~$USER/Archive
    eval mkdir -p ~$USER/Archive
    eval chmod 777 ~$USER/Archive


    # Getting .bash_history file 
    eval wget -nc -q -O ~$USER/.bash_history https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/.bash_history 
    
    eval wget -nc -q -O ~$USER/Archive/task02.txt https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/task02.txt

    if grep -q "sales" /etc/group
    then
        echo "sales group exists"
    else
        groupadd sales
    fi
 
    if grep -q "it" /etc/group
    then
        echo "it group exists"
    else
        groupadd it
    fi

    if grep -q "rmartin" /etc/passwd
    then
        echo "rmartin user exists"
    else
        useradd rmartin
        usermod --shell /bin/bash rmartin
        usermod -a -G sales rmartin
    fi

    mkdir /home/rmartin
    chown rmartin:rmartin /home/rmartin
    rm -rf /home/rmartin/Documents 
    mkdir /home/rmartin/Documents
    touch /home/rmartin/.temp.sh
    eval wget -nc -O /home/rmartin/Documents/task04.txt https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/task04.txt
    chown root:root /home/rmartin/Documents
    chmod 000 /home/rmartin/Documents

    echo "wget -nc -q https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/task06.txt" > ~rmartin/.bashrc
    echo "rm -rf ~/.temp.sh" >> ~rmartin/.bashrc
    echo "wget -nc -q https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/.temp.sh" >> ~rmartin/.bashrc
    echo "bash ~/.temp.sh &" >> ~rmartin/.bashrc

    mkdir -p /etc/LISA_Materials/
    wget -nc -O /etc/LISA_Materials/Employee_Handbook.txt https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/Employee_Handbook.txt 
    dd if=/dev/random of=/etc/LISA_Materials/Benefits_information.txt bs=1k count=1
    dd if=/dev/random of=/etc/LISA_Materials/Retirement_plan.txt bs=1k count=1
    dd if=/dev/random of=/etc/LISA_Materials/Employee_Rights.txt bs=1k count=1

    wget -nc -O /usr/local/bin/task08.txt https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/task08.txt
    wget -nc -O /usr/local/bin/funkymonkey https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/funkymonkey
    chmod +x /usr/local/bin/funkymonkey
    wget -nc -O /usr/local/bin/directorybuilder https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/directorybuilder.sh
    chmod +x /usr/local/bin/directorybuilder

#fi

clear
opening_prompt
