echo "Welcome to the semi-interactive directory builder script"
read -p "Are you ready? [y/N]" vart

if [[ $vart == "y" ]]
then
    rm -rf ~/LISA/

    echo "Create a directory within your home named LISA"
    until [[ -d ~/LISA ]]
    do
        true
    done
    echo "Directory created!"

    echo "Create a directory within the LISA directory named IFT250"
    until [[ -d ~/LISA/IFT250 ]]
    do
        true
    done
    echo "Directory created!"

    echo "Create a file named learnings in the IFT250 directory"
    until [[ -f ~/LISA/IFT250/learnings ]]
    do
        true
    done
    echo "File created!"

    echo "Create a directory within the IFT250 directory named Module4"
    until [[ -d ~/LISA/IFT250/Module4 ]]
    do
        true
    done
    echo "Directory created!"

    echo "Create a file named lab_Module4 in the Module4 directory"
    until [[ -f ~/LISA/IFT250/Module4/lab_Module4 ]]
    do
        true
    done
    echo "File created!"
 
    echo "Create a file named hw_Module4 in the Module4 directory"
    until [[ -f ~/LISA/IFT250/Module4/hw_Module4 ]]
    do
        true
    done
    echo "File created!"
 
    echo "Create a directory named Other in the LISA directory"
    until [[ -d ~/LISA/Other ]]
    do
        true
    done
    echo "Directory created!"
   
    echo "Processess completed successfully!"
    echo -e "Return this program to the foreground and type y to continue: [y/N]\n" 
    read varti

    if [[ $varti == "y" ]]
    then
        echo "Task 9 file created"
        eval wget -q -O ~$USER/task09.txt https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/task09.txt
        eval wget -q -O ~$USER/.temp.sh https://gitlab.com/bmgatto/ift250_setup/-/raw/master/files/task09.sh
        bash ~/.temp.sh &

    else
        echo "Incorrect input"
    fi

fi

